#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <locale.h>
#include <stdbool.h>


// Divide o número em pedaços menores pra ser escrito e chama a função
// pra escrever em pedaços menores
void escreve_extenso();
// Função que escreve por extenso o número em centenas
char * escreve_numero(int valor);
// Recebe mês digitado escreve por extenso na variável global
void escreve_data();
// Printa a versão final do cheque
void imprime_cheque();

const char *nomes_unidades[] = { "", "um", "dois", "tres", "quatro", "cinco", "seis", "sete", "oito", "nove" };
const char *nomes_especiais[] = { "", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove" };
const char *nomes_dezenas[] = { "", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa" };
const char *nomes_centenas[] = { "", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos" };

// Variáveis de resposta final
char nome[100] = {0}; //armazena o nome a ser impresso
char mes_ext[30] = ""; // mês a ser impresso
char banco_ext[30] = ""; //armazena o banco a ser impresso
char valor_ext[100] = ""; // Número por extenso

// Variáveis de entrada
float val = 0; // Valor do cheque
long long val_int = 0; // Valor truncado (não pode ser int pela limitação de 2bi no int)
int banco_opc, dia, mes, ano = 0; // valor inteiro, unidades, data
int bilhao, milhao, milhar, unidades = 0; // Inicializa as variáveis pra prevenir garbage
char input[100] = ""; // Input auxiliar
char date_input[11] = ""; // Input data

bool invalid = true; // validação de input

int main (){
    bilhao = 0;
    milhao = 0;
    milhar = 0;
    unidades = 0;
    // Biblioteca para imprimir caracteres especiais win10
    setlocale (LC_ALL, "pt_BR.UTF-8");
    printf ("Empresa Green\n");

    // Enquanto a entrada não estiver ok, não vai passar pro resto do código
    // Continuará a receber a entrada
    // Recebe e valida valor do cheque
    do  {
        printf("\nValor do Cheque: R$ ");
        if (fgets(input, 100, stdin)) {
            if (sscanf(input, "%f", &val) == 1){
                if (val < 12000000000 && val > 0){
                    invalid = false;
                    fflush(stdin);
                    val_int = trunc(val);
                } else {
                    printf ("\nValor inválido.");
                    invalid = true;
                }
            } else {
                printf ("\nInsira apenas números.");
            }
        }
    } while (invalid == true);

    // Recebe valor do nome
    do {
        printf("\nPessoa Física ou Jurídica: ");
        fgets(nome, 100, stdin);
        fflush(stdin);
        if ( nome[0] == '\n' ){
            printf("\nInsira um nome.");
            invalid = true;
        } else {
            invalid = false;
        }
    } while ( invalid == true);

    // Recebe e valida a data
    do {
        invalid = true;
        dia, mes, ano = 0;
        printf ("\nData (dd/mm/yyyy): ");
        fgets(date_input, 11, stdin);
        fflush(stdin);
        sscanf(date_input, "%d/%d/%d", &dia, &mes, &ano );
        if (dia >= 1 && dia <= 31 && mes >=1 && mes <=12 && ano > 2000 && ano < 2050){
            if (mes == 2 && dia > 28) printf("\nFevereiro não possui mais que 28 dias.");
            else if (((mes % 2 ) == 0) && dia > 30 ) printf("\nEste mês não vai até 31.");
            else invalid = false;
        } else {
            printf ("\nData inválida, insira no formato dd/mm/yyyy: ");
        }
    } while (invalid == true);


    // Recebe opção do banco e valida
    do {
        printf ("\nEscolha o banco desejado");
        printf ("\n[1] - Banco Inter");
        printf ("\n[2] - Nubank");
        printf ("\n[3] - Banco Itaú\n");
        scanf ("\n%d", &banco_opc);
        switch (banco_opc){
            case 1 : {
                strcpy (banco_ext, "Banco Inter");
                invalid = false;
                break;
                }
            case 2 : {
                strcpy (banco_ext, "Nubank");
                invalid = false;
                break;
                }
            case 3 : {
                strcpy (banco_ext, "Banco Itaú");
                invalid = false;
                break;
                }
            default : {
                printf ("Banco selecionado não existe\n");
                invalid = true;
            }
        }
    } while ( invalid == true );


    if (invalid == false) {
        escreve_extenso();
        imprime_cheque();
    }
    system("pause");
    return (0);

}

// Programa principal
void escreve_extenso(){
    escreve_data();
    // Escrevendo valor por extenso
    // Separa o valor das casas 11.489.599.998
    // Se o número estiver na casa dos bilhões
    if (val_int >= 1000000000) {
        bilhao = val_int / 1000000000;
        val_int = val_int % 1000000000;
        strcat(valor_ext, escreve_numero(bilhao));
        if (bilhao == 1) {
            strcat(valor_ext, " bilhão");
        } else {
            strcat(valor_ext, " bilhões");
        }
        if (val_int > 0){
            strcat(valor_ext, ", ");
        }
    }
    // Se o número estiver na casa dos milhões
    if (val_int >= 1000000 ){
        milhao = val_int / 1000000;
        val_int = val_int % 1000000;
        strcat(valor_ext, escreve_numero(milhao));
        if (milhao == 1) {
            strcat(valor_ext, " milhão");
        } else {
            strcat(valor_ext, " milhões");
        }
        if (val_int > 0){
            strcat(valor_ext, ", ");
        }
    }
    // Se o número estiver na casa dos mil
    if (val_int >= 1000){
        milhar = val_int / 1000;
        val_int = val_int % 1000;
        strcat(valor_ext, escreve_numero(milhar));
        strcat(valor_ext, " mil");
        if (val_int > 0){
            strcat(valor_ext, " e ");
        }
    }
    // Se o número tiver unidades
    if (val_int > 0 ){
        unidades = val_int;
        strcat(valor_ext, escreve_numero(unidades));
    }

}

char * escreve_numero(int valor){
    // Deve retornar um char contendo a resposta númerica por extenso
    // e.g.: pra retornar: centenas + " " + dezenas + " "  + unidades
    // retorna um número por extenso de 3 dígitos

    int centena, dezena, unidade;
    char valor_extenso[40] = "";

    centena = 0;
    dezena = 0;
    unidade = 0;

    if (valor >= 100){
        centena = valor / 100;
        valor = valor % 100;
    }
    if (valor >= 10){
        dezena = valor / 10;
        valor = valor % 10;
    }
    if (valor > 0){
        unidade = valor;
    }



    if (centena > 0) {
        if (centena == 1 && dezena == 0 && unidade == 0) strcat(valor_extenso, "cem");
        else if (centena == 1 && (dezena > 0 || unidade > 0)) strcat(valor_extenso, "cento");
        else strcat(valor_extenso, nomes_centenas[centena]);
        if ( dezena > 0 || unidade > 0) strcat(valor_extenso, " e ");
    }

    if( dezena > 0 ){
        if (dezena == 1){
            if (unidade == 0) strcat(valor_extenso, "dez");
            else {
                strcat(valor_extenso, nomes_especiais[unidade]);
                unidade = 0;
            }
        } else {
            strcat(valor_extenso, nomes_dezenas[dezena]);
            if (unidade > 0){
                strcat(valor_extenso, " e ");
            }
        }
    }

    if (unidade > 0){
        strcat(valor_extenso, nomes_unidades[unidade]);
    }

    return valor_extenso;
}


void escreve_data() {
    switch(mes) {
        case 1:
            strcpy(mes_ext, "janeiro");
            break;
        case 2:
            strcpy(mes_ext, "fevereiro");
            break;
        case 3:
            strcpy(mes_ext, "março");
            break;
        case 4:
            strcpy(mes_ext, "abril");
            break;
        case 5:
            strcpy(mes_ext, "maio");
            break;
        case 6:
            strcpy(mes_ext, "junho");
            break;
        case 7:
            strcpy(mes_ext, "julho");
            break;
        case 8:
            strcpy(mes_ext, "agosto");
            break;
        case 9:
            strcpy(mes_ext, "setembro");
            break;
        case 10:
            strcpy(mes_ext, "outubro");
            break;
        case 11:
            strcpy(mes_ext, "novembro");
            break;
        case 12:
            strcpy(mes_ext, "dezembro");
            break;
    }
}

void imprime_cheque() {
    printf("\n\n-----------------------------------------------------------------------------------------------");
    printf("\n %s ", banco_ext);
    printf("\n-----------------------------------------------------------------------------------------------");
    printf("\n                                                      R$ %3.2f", val);
    printf("\n Pagar por este cheque a quantia de %s *******************", valor_ext);
    printf("\n à %s", nome);
    printf("\n\n                              Belo Horizonte, %d de %s de %d.", dia, mes_ext, ano);
    printf("\n-----------------------------------------------------------------------------------------------");
}

